//
//  ProductCollectionViewCell.swift
//  TagSearch
//
//  Created by Gerson  on 22/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productQuantityLb: UILabel!
    @IBOutlet weak var productTitleLb: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    func configure(quantity: Int, product: Product) {
        productQuantityLb.text = "\(quantity) items"
        productTitleLb.text = product.title
        
        getImage(url: product.image.src)
        productImage.layer.cornerRadius = productImage.frame.width / 2
    }
    
    // MARK: - Download Image
    
    func getImage(url: String) {
        guard let imageUrl = URL(string: url) else { return }
        
        let task = URLSession.shared.dataTask(with: imageUrl) { (data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            guard let imgData = data else {
                print("Error: did not receive image data")
                
                return
            }
            
            DispatchQueue.main.async {
                self.productImage.image = UIImage(data: imgData)
            }
            
        }
        task.resume()
    }

}
