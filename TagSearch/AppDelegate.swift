//
//  AppDelegate.swift
//  TagSearch
//
//  Created by Gerson  on 20/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor(red: 95.0/255.0, green: 135.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        return true
    }

}

