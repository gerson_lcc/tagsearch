//
//  TagTableViewController.swift
//  TagSearch
//
//  Created by Gerson  on 22/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class TagTableViewController: UITableViewController {
    
    var products: [Product] = []
    var tags: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadProducts()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = tags[indexPath.row]

        return cell
    }
    
    // MARK: - Get JSON data
    
    func loadProducts() {
        
        let jsonAPI: String = "https://shopicruit.myshopify.com/admin/products.json?page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6"
        
        let url = URL(string: jsonAPI)!
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            
            guard error == nil else {
                print("Error getting data, error: \(error!)")
                return
            }
            // make sure we got data
            guard let jsonData = data else {
                print("Error: did not receive data")
                
                return
            }
            
            do {
                let productsList = try JSONDecoder().decode(ProductList.self, from: jsonData)
                self?.products = productsList.products
                
//                productsList.products.forEach { self?.getTags(tag: $0.tags) }
                
                for index in (productsList.products) {
                    self?.getTags(tag: index.tags)
                }
            } catch {
                print(error.localizedDescription)
                
                return
            }

            DispatchQueue.main.sync {
                self?.tableView.reloadData()
            }
        })
        task.resume()
    }
    
    func getTags(tag: String) {
        // Creates a parameter to separate multiple tags from a single String
        let stringSeparator = ", "
        let tempTags = tag.components(separatedBy: stringSeparator)
        
        for tag in tempTags {
            if !tags.contains(tag) {
                tags.append(tag)
            }
        }
        
//        let tagsToAdd = tempTags.filter { !tags.contains($0) }
//        tags.append(contentsOf: tagsToAdd)
        
        tags = tags.sorted()
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Sends an array of items with the selected tag to ProductCollectionViewController
        let destinationVC = segue.destination as! ProductsCollectionViewController
        
        guard let tagIndex = tableView.indexPathForSelectedRow?.row else { return }
        
        let productWithTag = products.filter { $0.tags.contains(tags[tagIndex]) }
        
//        for product in products {
//            if product.tags.contains(tags[tableView.indexPathForSelectedRow!.row]) {
//                productWithTag.append(product)
//            }
//        }
        
        //destinationVC.selectedTag = tags[tableView.indexPathForSelectedRow!.row]
        destinationVC.productList = productWithTag
    }
}
