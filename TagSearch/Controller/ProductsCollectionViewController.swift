//
//  ProductsCollectionViewController.swift
//  TagSearch
//
//  Created by Gerson  on 22/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class ProductsCollectionViewController: UICollectionViewController {

    var productList: [Product] = []
    //var selectedTag: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "\(productList.count) products"
        
        collectionView?.backgroundColor = UIColor(red: 244.0/255.0, green: 246.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 16, bottom: 10, right: 16)
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        if let productCollectionViewCell = cell as? ProductCollectionViewCell {
            // Check how many variants the product has
            let variantCount = productList[indexPath.row].variants.count
            var quantity = 0

            // Calculate the total amount from all variants for that product
            for variantIndex in 0..<variantCount {
                quantity += productList[indexPath.row].variants[variantIndex].quantity
            }
            
//            let quantity = productList[indexPath.row].variants.reduce(0) { (total, variant) in
//                return total + variant.quantity
//            }
            
            // Configure the cell
            productCollectionViewCell.configure(quantity: quantity, product: productList[indexPath.row])
        }
        
        return cell
    }
}
