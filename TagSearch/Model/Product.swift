//
//  Profile.swift
//  TagSearch
//
//  Created by Gerson  on 20/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

struct Product: Codable {
    let title: String
    let tags: String
    let variants: [Variant]
    let image: Image
}
