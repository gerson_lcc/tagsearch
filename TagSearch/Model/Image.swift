//
//  Image.swift
//  TagSearch
//
//  Created by Gerson  on 22/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

struct Image: Codable {
    let src: String
}
