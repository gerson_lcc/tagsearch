//
//  Variant.swift
//  TagSearch
//
//  Created by Gerson  on 22/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

struct Variant: Codable {
    let title: String
    let price: String
    let quantity: Int
    
    enum CodingKeys: String, CodingKey {
        case title
        case price
        case quantity = "inventory_quantity"
    }
}
